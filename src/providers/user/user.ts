import { Injectable } from '@angular/core';

import * as firebase from 'firebase';

import { Environment } from '../../environments/environment'; 
import { ConstantsProvider } from '../../providers';

@Injectable()
export class UserProvider {

  secondaryApp: any;

  constructor(private constants: ConstantsProvider) {
    this.secondaryApp = firebase.initializeApp(Environment.firebase, "Secondary");
  }

  async create(email) {
    return  this.secondaryApp
      .auth()
      .createUserWithEmailAndPassword(email.toLowerCase(), this.constants.PASSWORD);
  }

  async updateProfile(result: any, name: string){
    return new Promise((resolve, reject) => {
      result.user
       .updateProfile({displayName: name, photoURL: ""})
       .then(user => {
          resolve(user);
       })
       .catch(error => {
          reject(error);
          console.error(error);
       });
    });
  }

}
