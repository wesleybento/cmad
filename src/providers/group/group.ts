import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

import { Observable } from 'rxjs';
import * as firebase from 'firebase';

import { ConstantsProvider } from '../../providers';

@Injectable()
export class GroupProvider {

  constructor(public afs: AngularFirestore, private constants: ConstantsProvider) {}

  get(id): Observable<any> {
    return this.afs
      .collection(this.constants.PATHS.GROUPS)
      .doc(id)
      .valueChanges()  
  }

  async update(group: any) {
    return this.afs
      .collection(this.constants.PATHS.GROUPS)
      .doc(group.id)
      .update(group)
      .then(()=>{
        return {
          msg: "Grupo atualizado com sucesso."
        }
      })
      .catch(error =>{
        return {
          error: error
        }
      });
  }

  async makePresident(newPresidentUID: string, groupID?: string):Promise<any> {
    const makePresident = firebase
                            .functions()
                            .httpsCallable('makePresident');
    
    return makePresident({newPresidentUID: newPresidentUID, groupID: groupID});
  }

  async addOrganizer(organizerUID: string) {
    const addOrganizer = firebase
                          .functions()
                          .httpsCallable('addOrganizer');

    return addOrganizer({organizerUID: organizerUID});
  }

  async remove(uid: string, group: any) {
    await this.removeOrganizer(uid);
    group.organizers = group.organizers.filter(organizer => organizer.uid !== uid);
    return this.update(group);
  }

  async removeOrganizer(organizerUID: string) {
    const removeOrganizer = firebase
                              .functions()
                              .httpsCallable('removeOrganizer');  
    
    return removeOrganizer({organizerUID: organizerUID});
  }

  async scanStatus(members: any) {
    const scanStatus = firebase
                            .functions()
                            .httpsCallable('scanStatus');
    
    return scanStatus({members: members});
  }

}
