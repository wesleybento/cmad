import { Injectable } from '@angular/core';

@Injectable()
export class ConstantsProvider {

  public PASSWORD = "mudar123";

  public PATHS = { MEMBERS: "/members", 
                   GROUPS: "/groups", 
                   UPDATED_MEMBERS: "/updatedMembers",
                   LISTS: "/lists", 
                   UPLOADS: "/uploads" };

  public STATUS = { NEW: "novo", 
                    GUEST: "visitante", 
                    ACTIVE: "ativo", 
                    YELLOW: "amarelo",
                    RED: "vermelho",
                    INACTIVE: "inativo" };

  public PARAMS = { ASC: "asc", DESC: "desc" };

}
