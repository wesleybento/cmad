import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

import { Observable } from 'rxjs';

import { ConstantsProvider } from '../../providers';

@Injectable()
export class MemberProvider {

  constructor(private afs: AngularFirestore, private constants: ConstantsProvider) {}

  getAll(groupID): Observable<any[]> {
    return this.afs
      .collection(this.constants.PATHS.MEMBERS, ref => 
        ref.where('groupID', '==', groupID))
      .valueChanges();  
  }

  getUpdated(groupID): Observable<any[]> {
    return this.afs
      .collection(this.constants.PATHS.UPDATED_MEMBERS, ref => 
        ref.where('groupID', '==', groupID))
      .valueChanges();  
  }

  getByStatus(groupID, param): Observable<any[]> {
    return this.afs
      .collection(this.constants.PATHS.MEMBERS, ref => 
        ref.where('groupID', '==', groupID)
           .where("status", "==", param))
      .valueChanges();  
  }

  getOrdered(groupID, param): Observable<any[]> {
    return this.afs
      .collection(this.constants.PATHS.MEMBERS, ref => 
        ref.where('groupID', '==', groupID)
           .orderBy("lastPresence", param))
      .valueChanges();  
  }

  get(uid): Observable<any> {
    return this.afs
      .collection(this.constants.PATHS.MEMBERS)
      .doc(uid)
      .snapshotChanges()
      .map( result => {  
        const data = result.payload.data() as any;
        const id = result.payload.id;
        return {id, ...data};
      });  
  }

  async update(member: any ) {

    const timestamp = new Date();

    member.updated_at = timestamp;

    if(member.created_at == null) {
      member.created_at = timestamp;
    }

    if(member.id == null){
      member.id = timestamp.getTime().toString();
    }

    if(member.status == null) {
      member.status = this.constants.STATUS.NEW;
    }

    return this.afs
        .collection(this.constants.PATHS.MEMBERS)
        .doc(member.id)
        .set(member);
  }

  async delete(key: string) {
    return this.afs.collection(this.constants.PATHS.MEMBERS)
      .doc(key)
      .delete()
      .then(() => {
        return {
          msg: "Membro deletado com sucesso."
        }
      });  
  }
  
}
