import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file';
import { Platform } from 'ionic-angular';

import * as Json2csvParser from 'json2csv';

@Injectable()
export class ReportProvider {

  constructor(private file: File, private platform: Platform) {}

  parseMembers(data) {
    try {
      const fields = ['id', 'name', 'email', 'phone', 'status', 'birthdate', 'comments', 'integration'];
      const parser = new Json2csvParser.Parser({fields});
      const csv = parser.parse(data);
      return new Blob([csv], { type: 'text/csv' });
    } catch {
      return null;
    }
  }

  parseLists(data) {
    try {
      const fields = ['id', 'name', 'description', 'members.name', 'members.id'];
      const parser = new Json2csvParser.Parser({fields, unwind: ['members'], unwindBlank: true});
      const csv = parser.parse(data);
      return new Blob([csv], { type: 'text/csv' });
    } catch {
      return null;
    }
  }

  parseUpdatedMembers(data) {
    try {
      for(var i =0; i < data.length; i++){
        var dado = data[i];
        var timeAtt = this.convertSecondsInDateTime(dado.updated_at.seconds).getTime();
        var timeCreated = this.convertSecondsInDateTime(dado.created_at.seconds).getTime();
        data[i].DataCriacao = new Date(timeCreated).toLocaleDateString();
        data[i].DataAtualizacao = new Date(timeAtt).toLocaleDateString();
        data[i].StatusAnterior = dado.oldstatus;
        data[i].Nome = dado.name;
        data[i].UltimaPresencaAnterior = dado.oldLastPresence;
        data[i].UltimaPresenca = dado.lastPresence;
      }
      const fields = ['id', 'Nome', 'StatusAnterior', 'status', 'DataCriacao', 'UltimaPresencaAnterior', 'UltimaPresenca', 'DataAtualizacao'];
      const parser = new Json2csvParser.Parser({fields});
      const csv = parser.parse(data);
      return new Blob([csv], { type: 'text/csv' });
    } catch {
      return null;
    }
  }

  async createFile(blob) {
    let min = new Date().getMinutes();
    let sec = new Date().getSeconds();
    let mil = new Date().getMilliseconds();
    let fileName = `relatorio_${min}${sec}${mil}.csv`;
    let filePath = (this.platform.is('android')) ? this.file.externalRootDirectory : this.file.cacheDirectory;

    return this.file
      .writeFile(filePath, fileName, blob);
  }

  convertSecondsInDateTime(secs) : Date{
    var dt = new Date(1970, 0, 1);
    dt.setSeconds(secs);
    return dt;
  }
}