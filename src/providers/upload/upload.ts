import { Injectable } from '@angular/core';

import * as firebase from 'firebase';

import { Upload } from '../../models/upload.interface';
import { ConstantsProvider } from '../constants/constants';

@Injectable()
export class UploadProvider {

  constructor(private constants: ConstantsProvider) {}

  async uploadFromBrowser(file: File) {

    const upload = {file: file} as Upload;

    let storageRef = firebase.storage().ref();
    let uploadTask = storageRef.child(`${this.constants.PATHS.UPLOADS}/${upload.file.name}`).put(upload.file);

    return uploadTask.then((res:any) => {
      return res.ref
        .getDownloadURL()
        .then(url => {
          return {
            url
          }
        });
    });    
  }

  async uploadFromMobile(data) {
    let storageRef = firebase.storage().ref();
    const filename = Math.floor(Date.now() / 1000);
    const imageRef = storageRef.child(`${this.constants.PATHS.UPLOADS}/${filename}.jpg`);
    
    return imageRef
      .putString(data, firebase.storage.StringFormat.DATA_URL)
      .then((res:any) => {
        return res.ref
          .getDownloadURL()
          .then(url => {
            return {
              url
            }
          });
      });
  }
}
