import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';


@Injectable()
export class PictureProvider {

  cameraOptions: CameraOptions = {
    quality: 50,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    sourceType: 0
  };

  constructor(private camera: Camera) {}

  async getPicture() {
    return this.camera
      .getPicture(this.cameraOptions)
      .then((captureDataUrl) => {
          return {
            url: 'data:image/jpeg;base64,' + captureDataUrl
          } 
      });
  }

}
