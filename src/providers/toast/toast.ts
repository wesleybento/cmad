import { ToastController } from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class ToastProvider {

  constructor(public controller: ToastController) {}

  show(msg: string, cssClass: string) {
    this.controller
      .create({message: msg, 
        duration: 3000, 
        position: 'top', 
        dismissOnPageChange: false,
        cssClass: cssClass})
      .present();
  }

}
