import { AlertController } from 'ionic-angular';
import { Injectable } from '@angular/core';

@Injectable()
export class AlertProvider {

  constructor(public controller: AlertController) {}

  presentAlert(title?: string, msg?: string) {
    let alert = this.controller.create({
      title: title,
      subTitle: msg,
      buttons: ['Ok']
    });
    alert.present();
  }

  presentConfirm(title?: string, msg?: string, cancelAction?: any, confirmAction?: any) {
    let alert = this.controller.create({
      title: title,
      message: msg,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: cancelAction
        },
        {
          text: 'Confirmar',
          handler: confirmAction
        }
      ]
    });
    alert.present();
  }

  presentPrompt(inputs: Array<any>, title?: string, cancelAction?: any, confirmAction?: any) {
    let alert = this.controller.create({
      title: title,
      inputs: inputs,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: cancelAction
        },
        {
          text: 'Confirmar',
          handler: data => {
            confirmAction(data)
          }
        }
      ]
    });
    alert.present();
  }

  // prompt input examples, do not erase

  // inputs: [
  //   {
  //     name: 'username',
  //     placeholder: 'Username'
  //   },
  //   {
  //     name: 'password',
  //     placeholder: 'Password',
  //     type: 'password'
  //   }
  // ],
}
