import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as firebase from 'firebase';

import { ConstantsProvider } from '../../providers';

@Injectable()
export class AttendanceListProvider {

  constructor(private afs: AngularFirestore, private constants: ConstantsProvider) {}

  getAll(groupID): Observable<any[]> {
    return this.afs
      .collection(this.constants.PATHS.LISTS, ref => 
        ref.where('groupID', '==', groupID))
      .valueChanges();  
  }

  getOrdered(groupID, param): Observable<any[]> {
    return this.afs
      .collection(this.constants.PATHS.LISTS, ref => 
        ref.where('groupID', '==', groupID)
           .orderBy("created_at", param))
      .valueChanges();  
  }

  get(id){
    return this.afs
      .collection(this.constants.PATHS.LISTS)
      .doc(id)
      .snapshotChanges()
      .pipe(
        map( result => {  
          const data = result.payload.data() as any;
          const id = result.payload.id;
          return {id, ...data};
        })
      );  
  }

  async update(list: any ) {

    const timestamp = firebase.firestore.FieldValue.serverTimestamp();

    list.updated_at = timestamp;

    if(list.created_at == null) {
      list.created_at = timestamp;
    }

    if(list.id == null) {
      list.id = new Date().getTime().toString();
    }

    return this.afs
      .collection(this.constants.PATHS.LISTS)
      .doc(list.id)
      .set(list);
  }

  async delete(key: string) {
    this.afs.collection(this.constants.PATHS.LISTS)
      .doc(key)
      .delete()
      .then(() => {
        return {
          msg: "Lista deletada com sucesso."
        }
      });  
  }
  
}
