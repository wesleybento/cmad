import { AppModule } from "../app/app.module";
import { AngularFireAuth } from 'angularfire2/auth'

export function AuthChecker() {

    return function (constructor) {
        constructor.prototype.ionViewWillEnter = function() {
            const auth = AppModule.injector.get(AngularFireAuth);
            
            if(auth.auth.currentUser == null){
                this.navCtrl.setRoot('LoginPage');
            }
            
        }
    }

}