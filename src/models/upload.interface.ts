export interface Upload {
    $key: string;
    file: File;
    name: string;
    createdAt: Date;
}