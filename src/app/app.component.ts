import { Component } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Platform } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  template: `<ion-nav #content [root]="rootPage"></ion-nav>`
})
export class MyApp {
  
  public currentUser: any;
  
  public isPresident: any;
  
  public groupID: string;
  
  public rootPage: any;

  constructor(private platform: Platform,
              private statusBar: StatusBar, 
              private splashScreen: SplashScreen,
              private auth: AngularFireAuth) {

    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });

    this.auth.authState
      .subscribe(res => {
        
        if(res != null) {

          this.currentUser = this.auth.auth.currentUser;

          this.auth
            .auth
            .currentUser
            .getIdTokenResult()
            .then(idTokenResult => {
              
              this.isPresident = idTokenResult.claims.isPresident;
              this.groupID = idTokenResult.claims.groupID;
              
              if(this.groupID != undefined && this.groupID != null) {
                this.rootPage = 'HomePage';
              } else {
                this.rootPage = 'LoginPage';
              }
              
            });

        }else{
          this.rootPage = 'LoginPage'
        }

      }, error => {
        console.error(error);
      })
  }
  
}
