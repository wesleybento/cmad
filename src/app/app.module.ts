import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule, Injector } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule } from '@ionic/storage';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { File } from '@ionic-native/file';

import { Environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { MyApp } from './app.component';
import { MemberProvider } from '../providers/member/member';
import { AttendanceListProvider } from '../providers/attendance-list/attendance-list';
import { ConstantsProvider } from '../providers/constants/constants';
import { UserProvider } from '../providers/user/user';
import { UploadProvider } from '../providers/upload/upload';
import { ToastProvider } from '../providers/toast/toast';
import { AlertProvider } from '../providers/alert/alert';
import { GroupProvider } from '../providers/group/group';
import { ReportProvider } from '../providers/report/report';
import { PictureProvider } from '../providers/picture/picture';


@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: ''
    }),
    AngularFireModule.initializeApp(Environment.firebase),
    AngularFirestoreModule.enablePersistence(),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    Camera,
    SplashScreen,
    StatusBar,
    File,
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    MemberProvider,
    AttendanceListProvider,
    ConstantsProvider,
    UserProvider,
    UploadProvider,
    ToastProvider,
    AlertProvider,
    GroupProvider,
    ReportProvider,
    PictureProvider
  ]
})
export class AppModule {
  static injector: Injector;

  constructor(injector: Injector){
    AppModule.injector = injector;
  }
}
