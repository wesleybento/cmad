import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ConstantsProvider, ToastProvider, AlertProvider } from '../../providers';
import { MemberProvider } from '../../providers';
import { Subject } from 'rxjs';
import { MyApp } from '../../app/app.component';

@IonicPage()
@Component({
  selector: 'page-members-list',
  templateUrl: 'members-list.html',
})
export class MembersListPage {

  stop$: Subject<boolean> = new Subject<boolean>();
  loading: boolean = false;
  filteredMembers = [];
  allMembers = [];
  searchTerm: string;
  groupID: any;

  constructor(private constants: ConstantsProvider,
              private myApp: MyApp,
              private provider: MemberProvider, 
              private toast: ToastProvider,
              private alert: AlertProvider,
              public navCtrl: NavController, 
              public navParams: NavParams) {
               
    this.groupID = this.myApp.groupID;            
  }

  ionViewDidEnter() {
    this.provider
      .getAll(this.groupID)
      .takeUntil(this.stop$)
      .subscribe(res => {
        this.allMembers = res;
        this.filteredMembers = res;
      }, error => {
        console.error(error);
      });
  }  

  filterStatus(param) {
    this.provider
      .getByStatus(this.groupID, param)
      .takeUntil(this.stop$)
      .subscribe(res => {
        this.allMembers = res;
        this.filteredMembers = res;
      }, error => {
        console.error(error);
      });
  }

  filterOrder(param) {
    this.provider
      .getOrdered(this.groupID, param)
      .takeUntil(this.stop$)
      .subscribe(res => {
        this.allMembers = res;
        this.filteredMembers = res;
      }, error => {
        console.error(error);
      });
  }

  setFilteredMembers(){
    this.filteredMembers = this.filterItems();
  }

  filterItems(){
    return this.allMembers.filter((member) => {
      return member.name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
    });
  }

  openPage(page, member) {
    this.navCtrl.push(page, {
      member: member
    });
  }

  confirmDelete(member) {
    this.alert
      .presentConfirm("Atenção", 
                      "Deseja deletar este membro?", 
                      null, 
                      ()=>{ this.delete(member) });
  }

  delete(member){
    this.loading = true;

    this.provider
      .delete(member.id)
      .then(res => {
        this.loading = false;
        this.toast.show(res.msg, "success-msg");
      })
      .catch(error => {
        this.loading = false;
        this.toast.show(error, "success-msg");
      });
  }

  ionViewWillLeave() {
    this.stop$.next(true);
  }

}
