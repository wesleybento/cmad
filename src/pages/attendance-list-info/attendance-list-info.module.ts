import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AttendanceListInfoPage } from './attendance-list-info';

@NgModule({
  declarations: [
    AttendanceListInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(AttendanceListInfoPage),
  ],
})
export class AttendanceListInfoPageModule {}
