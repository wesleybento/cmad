import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-attendance-list-info',
  templateUrl: 'attendance-list-info.html',
})
export class AttendanceListInfoPage {

  list: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if(this.navParams.get('list') != null) {
      this.list = this.navParams.get('list');
    }
  }

  openPage(page, member) {
    this.navCtrl.push(page, {
      list: this.list,
      member: member
    });
  }

}
