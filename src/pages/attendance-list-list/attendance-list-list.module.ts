import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AttendanceListListPage } from './attendance-list-list';

@NgModule({
  declarations: [
    AttendanceListListPage,
  ],
  imports: [
    IonicPageModule.forChild(AttendanceListListPage),
  ],
})
export class AttendanceListListPageModule {}
