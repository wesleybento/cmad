import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MyApp } from '../../app/app.component';

import { Subject } from 'rxjs';

import { AttendanceListProvider, ToastProvider, AlertProvider, ConstantsProvider } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-attendance-list-list',
  templateUrl: 'attendance-list-list.html',
})
export class AttendanceListListPage {

  groupID: any;
  loading: boolean = false;
  stop$: Subject<boolean> = new Subject<boolean>();
  lists = [];

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private provider: AttendanceListProvider,
              private toast: ToastProvider,
              private alert: AlertProvider,
              private constants: ConstantsProvider,
              private myApp: MyApp  ) {

    this.groupID = this.myApp.groupID;            
  }

  ionViewDidEnter(){
    this.provider
      .getAll(this.groupID)
      .takeUntil(this.stop$)
      .subscribe(res => {
        for(var i =0; i < res.length; i++)
          res[i].presencas = res[i].members.length;

        this.lists = res;
      }, error => {
        console.error(error);
      });
  }

  filterOrder(param) {
    this.provider
      .getOrdered(this.groupID, param)
      .takeUntil(this.stop$)
      .subscribe(res => {
        for(var i =0; i < res.length; i++)
          res[i].presencas = res[i].members.length;
          
        this.lists = res;
      }, error => {
        console.error(error);
      });
  }

  openPage(page, list) {
    this.navCtrl.push(page, {
      list: list
    });
  }

  confirmDelete(list) {
    this.alert
      .presentConfirm("Atenção", 
                      "Deseja deletar esta lista?", 
                      null, 
                      ()=>{ this.delete(list) });
  }

  delete(list){
    this.loading = true;

    this.provider
      .delete(list.id)
      .then((res: any) => {
        this.loading = false;
        this.toast.show(res.msg, "success-msg");
      })
      .catch(error => {
        this.loading = false;
        this.toast.show(error, "error-msg");
      });
  }

  ionViewWillLeave(){
    this.stop$.next(true);
  }
}
