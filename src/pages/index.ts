export const FirstRunPage = 'LoginPage';
export const MainPage = 'DashboardPage';
export const ContextsList = 'ContextsPage';
export const ContextDetail = 'ContextPage';
export const WordsList = 'WordsPage';
export const WordDetail = 'WordPage';