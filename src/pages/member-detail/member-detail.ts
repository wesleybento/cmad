import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';

import { MyApp } from '../../app/app.component';
import { ConstantsProvider } from '../../providers';
import { MemberProvider } from '../../providers';
import { ToastProvider } from '../../providers';
import { UploadProvider } from '../../providers';
import { PictureProvider } from '../../providers';
import { Upload } from '../../models/upload.interface';


@IonicPage()
@Component({
  selector: 'page-member-detail',
  templateUrl: 'member-detail.html',
})
export class MemberDetailPage {

  loading: boolean = false;
  isBrowser = true;
  title = "Criar Membro";
  member: any = {};
  pictureSrc: any;
  pictureFile: File;
  currentUpload: Upload;
  msg = "Membro adicionado com sucesso.";

  constructor(private constants: ConstantsProvider,
              private memberProvider: MemberProvider,
              private myApp: MyApp,
              private sanitizer: DomSanitizer,
              private platform: Platform,
              private upload: UploadProvider,
              private toast: ToastProvider,
              private picture: PictureProvider, 
              public navCtrl: NavController, 
              public navParams: NavParams) {

    if(this.platform.is('cordova')){
      this.isBrowser = false;
    }

    if(this.navParams.get('member') != null) {
      this.member = this.navParams.get('member');
      this.title = "Editar Membro"
      this.msg = "Membro editado com sucesso.";
    }            
  } 

  getPicture(){
    this.picture.getPicture()
      .then((res: any) => {
        this.pictureSrc = res.url;
      })
      .catch(error => {
        this.toast.show(error, "error-msg");
      });
  }

  onDateSelected(event){
    this.member.birthdate = event;
  }

  preUpdate() {
    if(this.member.name == null || this.member.email == null) {
      this.toast.show("Nome e Email são obrigatórios.", "error-msg");
      return;
    }
    
    this.loading = true;

    this.uploadPicture()
      .then(res => {
        if(res != null){
          this.member.picture = res.url;
        }
        this.updateMember();
      });

  } 

  updateMember() {

    this.member.groupID = this.myApp.groupID;
    
    this.memberProvider
          .update(this.member)
          .then(()=>{
            this.toast.show(this.msg, "success-msg");
            this.loading = false;
            this.navCtrl.pop();
          })
          .catch(error => {
            console.error(error);
            this.loading = false;
            this.toast.show(error, "error-msg");
          });
  }

  detectFile(event){
    this.pictureFile = event.target.files.item(0);
    this.pictureSrc = this.sanitizer
      .bypassSecurityTrustUrl(window.URL.createObjectURL(event.target.files.item(0)));
  }

  async uploadPicture() {

    if(this.pictureFile == null && this.pictureSrc == null) {
      return;
    }

    if(this.isBrowser == true) {
      return this.upload.uploadFromBrowser(this.pictureFile);
    } else {
      return this.upload.uploadFromMobile(this.pictureSrc);
    }
  }

}
