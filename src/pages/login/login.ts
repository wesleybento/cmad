import { Component } from '@angular/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { ToastProvider } from '../../providers/toast/toast';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  loading: boolean = false;

  account: { email: string, password: string } = {
    email: "",
    password: ""
  };

  constructor(public navCtrl: NavController,
              private auth: AngularFireAuth,
              private toast: ToastProvider,
              public toastCtrl: ToastController) {}

  
  doLogin() {
    
    this.loading = true;

    this.auth
      .auth
      .signInWithEmailAndPassword(this.account.email, this.account.password)
      .then(()=>{
        this.loading = false;
      })
      .catch(error => {
        this.loading = false;
        this.toast.show(error, "error-msg");
        console.error(error);
      });
  }

  openPage(page) {
    this.navCtrl.push(page);
  }

}
