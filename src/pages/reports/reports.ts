import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { DomSanitizer } from '@angular/platform-browser';

import { Subject } from 'rxjs';

import { MemberProvider, AttendanceListProvider } from '../../providers';
import { MyApp } from '../../app/app.component';
import { ToastProvider } from '../../providers/toast/toast';
import { ReportProvider } from '../../providers/report/report';
import { GroupProvider } from '../../providers/group/group';

@IonicPage()
@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html',
})
export class ReportsPage {

  loading: boolean = false;
  isBrowser:boolean = true;
  currentEmail: string;
  url: any;
  groupID: any;
  allMembers = [];
  allLists = [];
  updatedMembers = [];
  organizers = [];
  sendToEverybody = false;

  stop$: Subject<boolean> = new Subject<boolean>();

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private toast: ToastProvider,
              private report: ReportProvider,
              private group: GroupProvider,
              private myApp: MyApp,
              private sanitizer: DomSanitizer,
              private platform: Platform,
              private email: EmailComposer,
              private lists: AttendanceListProvider,
              private members: MemberProvider) {

    if(this.platform.is('cordova')){
      this.isBrowser = false;
    }
    
    this.groupID = this.myApp.groupID;

    this.currentEmail = this.myApp.currentUser.email;
  }

  ionViewDidLoad() {
    this.loading = true;

    this.members
      .getAll(this.groupID)
      .takeUntil(this.stop$)
      .subscribe(res => {
        this.loading = false;
        this.allMembers = res;
      }, error => {
        console.error(error);
      });
    
    this.lists
      .getAll(this.groupID)
      .takeUntil(this.stop$)
      .subscribe(res => {
        this.allLists = res;
      }, error => {
        console.error(error);
      });
      
    this.members
      .getUpdated(this.groupID)
      .takeUntil(this.stop$)
      .subscribe(res => {
        this.updatedMembers = res;
      }, error => {
        console.error(error);
      });

    this.group
      .get(this.groupID)
      .takeUntil(this.stop$)
      .subscribe(res => {
        this.organizers = res.organizers;
      }, error => {
        console.error(error);
      });  
  }

  exportCsv(data){
    
    this.loading = true;
    
    let blob;

    if(data == 'membros') {
      blob = this.report.parseMembers(this.allMembers);
    } else if(data == 'listas') {
      blob = this.report.parseLists(this.allLists);
    } else {
      blob = this.report.parseUpdatedMembers(this.updatedMembers);
    }
    
    if(this.isBrowser == true && blob != null) {
      this.url = this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(blob).replace(/%3A/g, ':'));
      this.loading = false;
    } else {
      this.report
        .createFile(blob)
        .then(res => {
          this.url = res.nativeURL;
          this.loading = false;
        })
        .catch(error =>{
          this.loading = false;
          this.toast.show(error, "error-msg");
          console.error(error);
        });
    }
  }

  sendEmail(){

    if(this.sendToEverybody == true) {
      this.currentEmail = this.getAllEmails();
    }

    this.platform.ready().then(()=>{
        this.email.isAvailable().then(() =>{
          let email = {
            to: this.currentEmail,
            attachments: [this.url]
          };
          this.loading = false;
          this.email.open(email);
        });
      });
  }

  getAllEmails(): string {

    this.organizers.forEach(organizer => {
      this.currentEmail = `${this.currentEmail}; ${organizer.email}`;
    });

    return this.currentEmail;
  }

  ionViewWillLeave(){
    this.stop$.next(true);
  }

}
