import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ReportsPage } from './reports';

import { EmailComposer } from '@ionic-native/email-composer';

@NgModule({
  declarations: [
    ReportsPage,
  ],
  imports: [
    IonicPageModule.forChild(ReportsPage),
  ],
  providers: [
    EmailComposer
  ]
})
export class ReportsPageModule {}
