import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { MemberProvider, AttendanceListProvider } from '../../providers';
import { Subject } from 'rxjs';
import { MyApp } from '../../app/app.component';
import { ToastProvider } from '../../providers/toast/toast';

@IonicPage()
@Component({
  selector: 'page-attendance-list-detail',
  templateUrl: 'attendance-list-detail.html',
})
export class AttendanceListDetailPage {

  title = "Nova Lista";
  msg = "Lista adicionada com sucesso.";
  groupID: any;
  loading: boolean = false;
  stop$: Subject<boolean> = new Subject<boolean>();
  list: any = {name: "", description: "", date: "", members:[]};
  filteredMembers = [];
  allMembers = [];
  searchTerm: string;
  public isPostBack : boolean = false;
  private msgErro : string = 'Oops, infelizmente houve um erro, fale com o responsável pelo sistema.';

  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private myApp: MyApp,
              private toast: ToastProvider,
              private listProvider: AttendanceListProvider,
              private members: MemberProvider) {
    
    this.isPostBack = false;
    this.groupID = this.myApp.groupID;
    if(this.navParams.get('list') != null) {
      this.list = this.navParams.get('list');
      this.title = "Editar Lista";
      this.msg = "Lista editada com sucesso.";
      this.obterMembros();
    }
  }

  ngOnInit(){
    if(!this.loading && this.list.id != undefined){
      this.loading = true;
      this.listProvider.get(this.list.id)
      .subscribe(
        ret => {

          if(this.isPostBack || (ret.Editing == true && ret.UsuarioEditor == this.myApp.currentUser.email)){
            this.loading = false;
            return;
          }

          if(ret.Editing != undefined && ret.Editing == true){
            this.toast.show('A Chamada está sendo editada', "error-msg");
            this.navCtrl.pop();
          }else{
            this.toast.show('Um momento enquanto garanto seu direito a edição', "info-msg");
            this.list.Editing = true;
            this.list.UsuarioEditor = this.myApp.currentUser.email;

            this.listProvider
            .update(this.list)
            .then(()=>{
              this.loading = false;
              this.isPostBack = true;
              this.toast.show('Tudo pronto para sua edição.', "success-msg");
            })
            .catch(error => {
              this.loading = false;
              this.toast.show(this.msgErro, "error-msg");
            });
          }
        });
    }
  }

  obterMembros(){
    this.members
    .getAll(this.groupID)
    .takeUntil(this.stop$)
    .subscribe(res => {
      var resFiltrada = res.filter((member) => {
        return this.list.members.filter(x=> x.id == member.id).length == 0
      });

      this.allMembers = resFiltrada;
      this.filteredMembers = resFiltrada;
    }, error =>{
      this.toast.show(this.msgErro, "error-msg");
    });
  }

  setFilteredMembers(){
    this.filteredMembers = this.filterItems();
  }

  filterItems(){
    return this.allMembers.filter((member) => {
      var filtrado = (member.name.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1) && this.list.members.filter(x=> x.id == member.id).length == 0;
      return filtrado;
    });
  }

  addMember(member) {
    this.list.members.push(member);
    
    this.filteredMembers = this.filteredMembers.filter((m) => {
      return member.id != m.id;
    });
  }

  removeMember(member) {
    this.list.members = this.list.members.filter((m) => {
      return member.id != m.id;
    });
    
    this.filteredMembers.push(member);
  }

  updateList() {
    this.loading = true;
    this.list.groupID = this.groupID;
    this.listProvider
      .update(this.list)
      .then(()=>{
        this.toast.show(this.msg, "success-msg");
        this.loading = false;
        this.isPostBack = false;
        this.navCtrl.pop();
      })
      .catch(error => {
        console.error(error);
        this.loading = false;
        this.toast.show(error, "error-msg");
      });
  }

  openPage(page) {
    this.navCtrl.push(page);
  }

  ionViewWillLeave(){
    this.loading = true;
    this.isPostBack = true;

    if(this.list.Editing == true){
      this.list.Editing = false;
      this.listProvider
      .update(this.list)
      .then(()=>{
        this.loading = false;
        this.stop$.next(true);
      })
      .catch(error => {
        this.loading = false;
        this.toast.show(this.msgErro, "error-msg");
      });
    }
  }

}
