import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AttendanceListDetailPage } from './attendance-list-detail';

@NgModule({
  declarations: [
    AttendanceListDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(AttendanceListDetailPage),
  ],
})
export class AttendanceListDetailPageModule {}
