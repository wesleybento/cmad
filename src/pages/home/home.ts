import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthChecker } from '../../decorators/auth-checker';

import { AngularFireAuth } from 'angularfire2/auth';
import { Subject } from 'rxjs';

import { MyApp } from '../../app/app.component';
import { GroupProvider } from '../../providers/group/group';
import { MemberProvider, AttendanceListProvider } from '../../providers';

@IonicPage()
@AuthChecker()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  currentGroup: any = {};
  groupID: any;
  stop$: Subject<boolean> = new Subject<boolean>();
  stopScan$: Subject<boolean> = new Subject<boolean>();
  public members : Array<any> = new Array();
  constructor(private myApp: MyApp,
              private group: GroupProvider,
              private member: MemberProvider,
              private afAuth: AngularFireAuth,
              public navCtrl: NavController, 
              public navParams: NavParams,
              private chamadaProvider : AttendanceListProvider) {
    this.groupID = this.myApp.groupID;

    this.member
      .getAll(this.groupID)
      .takeUntil(this.stopScan$)
      .subscribe(res => {
        this.group.scanStatus(res);
        this.stopScan$.next(true);
        this.scanStatus();
      }, error => {
        console.error(error);
      });
  }

  async scanStatus(){
    if(this.groupID != undefined){
      this.member
      .getAll(this.groupID)
      .takeUntil(this.stopScan$)
      .subscribe(members => {
        this.members = members;

        this.chamadaProvider
        .getAll(this.groupID)
        .takeUntil(this.stop$)
        .subscribe(res => {
          const dayInMills = 86400000;
          const timestamp = new Date();
          
          for(var i = 0; i < this.members.length; i++){
            var presencas = 0;
            var member = this.members[i];
            member.oldLastPresence = member.lastPresence;

            const semestre = dayInMills*180;
            var ultimasChamadas = res.filter(x=> (new Date().getTime() - new Date(x.date).getTime()) < semestre);

            for(var j = 0; j < ultimasChamadas.length; j++){
              var presente = ultimasChamadas[j].members.filter(r=> r.id == member.id).length > 0;
              
              if(presente){
                presencas++;

                if(new Date(ultimasChamadas[j].date).getTime() > new Date(member.lastPresence).getTime())
                  member.lastPresence = ultimasChamadas[j].date;
              }
            }

            const createdAt = this.convertSecondsInDateTime(member.created_at.seconds).getTime();
            const lastPresence = new Date(member.lastPresence).getTime();
            const now = new Date().getTime();
            const membershipSpan = now - createdAt;
            const interval = now - lastPresence;
            const mes = dayInMills*30;
            const bimestre = dayInMills*60;
            const trimestre = dayInMills*90;
            member.oldstatus = member.status;
            member.updated_at = timestamp;

            if(membershipSpan < dayInMills*60){
              member.status = "novo";
            }else {
              if(presencas < 3)
                member.status = "visitante";
              else if(presencas >= 3){
                if(interval < mes) {
                  member.status = "ativo";
                } else if(interval > mes && interval < bimestre) {
                  member.status = "amarelo";
                } else if(interval > bimestre && interval < trimestre) {
                  member.status = "vermelho";
                } else if(interval > trimestre) {
                  member.status = "inativo";
                }
              }
            }
          }

          this.group.scanStatus(this.members);
          this.stopScan$.next(true);
        }, error => {
          console.error(error);
        });

      }, error => {
        console.error(error);
      });
    }
  }

  ionViewDidEnter(){
    this.group
      .get(this.groupID)
      .takeUntil(this.stop$)
      .subscribe(res => {
        this.currentGroup = res;
      }, error => {
        console.error(error);
      });
  }

  openPage(page){
    try{
      this.navCtrl.push(page);
    }
    catch(err){
      console.log(err);
    }
  }

  logout(){
    this.afAuth.auth.signOut();
  }

  ionViewWillLeave(){
    this.stop$.next(true);
  }

  convertSecondsInDateTime(secs) : Date{
    var dt = new Date(1970, 0, 1);
    dt.setSeconds(secs);
    return dt;
  }
}