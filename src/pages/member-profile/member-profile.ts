import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-member-profile',
  templateUrl: 'member-profile.html',
})
export class MemberProfilePage {

  member: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    if(this.navParams.get('member') != null){
      this.member = this.navParams.get('member');
    }
  }

  openPage(page){
    this.navCtrl.push(page, {
      member: this.member
    });
  }
}
