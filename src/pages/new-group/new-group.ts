import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFireAuth } from 'angularfire2/auth';
import { ToastProvider } from '../../providers/toast/toast';
import { GroupProvider } from '../../providers/group/group';
import { UserProvider } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-new-group',
  templateUrl: 'new-group.html',
})
export class NewGroupPage {

  loading: boolean = false;

  account: { displayName: string, email: string, password: string, password_match: string } = {
    displayName: null,
    email: null,
    password: null,
    password_match: null
  };

  constructor(private auth: AngularFireAuth,
              private toast: ToastProvider,
              private group: GroupProvider,
              private user: UserProvider,
              public navCtrl: NavController, 
              public navParams: NavParams) {
  }

  signup() {

    if(this.checkFields() == false) {
      this.toast.show("Verifique suas informações.", "error-msg");
      return;
    }

    this.loading = true;

    this.auth
      .auth
      .createUserWithEmailAndPassword(this.account.email, this.account.password)
      .then(res => {

        const promises = [];

        promises.push(this.user.updateProfile(res, this.account.displayName));

        promises.push(this.group.makePresident(res.user.uid));

        Promise.all(promises)
          .then(()=>{
            this.loading = false;
            this.toast.show("Grupo criado com sucesso.", "success-msg");
            this.auth.auth.signOut();
            this.navCtrl.pop();
          });
      }) 
      .catch(error => {
        this.loading = false;
        this.toast.show(error, "error-msg");
        console.error(error);
      });                               
  }

  checkFields(): boolean {
    
    var valid: boolean = true;

    if(this.account.displayName == null ||
       this.account.email == null ||
       this.account.password == null ||
       this.account.password_match == null ) {

      valid = false;    
    }

    if(this.account.password != this.account.password_match) {
      valid = false;
    }
    
    return valid;
  }

}
