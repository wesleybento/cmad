import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserProvider } from '../../providers';
import { ToastProvider } from '../../providers/toast/toast';
import { AlertProvider } from '../../providers/alert/alert';
import { GroupProvider } from '../../providers/group/group';
import { MyApp } from '../../app/app.component';
import { Subject } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  loading: boolean = false;

  groupID: string;

  currentUserUID: string;
  currentGroup: any = {};
  organizers = [];

  stop$: Subject<boolean> = new Subject<boolean>();
  
  constructor(public navCtrl: NavController, 
              public navParams: NavParams,
              private myApp: MyApp,
              private afAuth: AngularFireAuth,
              private alert: AlertProvider,
              private toast: ToastProvider,
              private group: GroupProvider,
              private user: UserProvider) {

    this.currentUserUID = this.afAuth.auth.currentUser.uid;
    this.groupID = this.myApp.groupID;
  }

  ionViewWillEnter(){
    this.group
      .get(this.groupID)
      .takeUntil(this.stop$)
      .subscribe(res => {
        this.currentGroup = res;
        this.organizers = res.organizers.filter(organizer => organizer.uid !== this.currentUserUID);
      }, error => {
        console.error(error);
      });
  }

  onAddUserClick(){

    const inputs = [{
                      name: 'name',
                      placeholder: 'Nome'
                    },
                    {
                      name: 'email',
                      placeholder: 'E-mail',
                      type: 'email'
                    }
                  ];
    
    const confirmAction = data => { this.createUser(data) }              

    this.alert.presentPrompt(inputs, "Adicionar Organizador", null, confirmAction);
  }  

  createUser(data: any) {

    this.loading = true;

    this.user
      .create(data.email)
      .then(result => {

        const promises = [];

        promises.push(this.user.updateProfile(result, data.name));
        
        promises.push(this.group.addOrganizer(result.user.uid));
        
        Promise.all(promises)
          .then(()=>{

            const organizer = {uid: result.user.uid, 
                               email: data.email, 
                               name: data.name};

            this.currentGroup.organizers.push(organizer);

            this.group
              .update(this.currentGroup)
              .then(() => {
                this.loading = false;
                this.toast.show("Organizador adicionado com sucesso.", "success-msg");    
              });
          }); 
        
      })
      .catch(error => {
        console.error(error);
        this.loading = false;
        this.toast.show(error, "error-msg");
      });
  }

  confirmRemove(organizerUID: string) {  
    this.alert
      .presentConfirm("Atenção", "Deseja remover este organizador?", null, ()=>{ 
        
        this.loading = true;

        this.group
          .remove(organizerUID, this.currentGroup)
          .then((result: any) => {
            this.loading = false;
            this.toast.show(result, 'error-msg');
          })
          .catch(error => {
            this.loading = false;
            this.toast.show(error, 'error-msg');
            console.error(error);
          }); 
      });
  }

  confirmPromote(organizerUID: string) {  
    this.alert
      .presentConfirm("Deseja promover este organizador a presidente?", "Será necessário fazer logout e login no sistema para esta alteração fazer efeito.", null, ()=>{ 
        
        this.loading = true;

        this.group
          .makePresident(organizerUID, this.groupID)
          .then((result: any) => {
            this.loading = false;
            this.toast.show(result.data.msg, 'success-msg');
          })
          .catch(error => {
            this.loading = false;
            this.toast.show(error.msg, 'error-msg');
            console.error(error);
          }); 
      });
  }

  save(){
    this.loading = true;
    this.group
      .update(this.currentGroup)
      .then((result: any) => {
        this.loading = false;
        this.toast.show(result.msg, "success-msg");
      })
      .catch(error => {
        this.loading = false;
        this.toast.show(error, "error-msg");
      });
  }

  ionViewWillLeave(){
    this.stop$.next(true);
  }

}
