import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

admin.initializeApp(functions.config().firebase);

exports.makePresident = functions.https.onCall((data, context) => {

    const currentPresidentUID = context.auth.uid;
    const isPresident = context.auth.token.isPresident;
    let groupID: string;

    if(data.groupID === undefined || data.groupID === null) {
        groupID = `${new Date().getMinutes()}${new Date().getSeconds()}${new Date().getMilliseconds()}`;
    } else {
        groupID = data.groupID;
    }

    if(isPresident === undefined || isPresident === null) {

        const promises = [];
        promises.push(grantPresidentRole(currentPresidentUID, groupID));
        promises.push(createGroup(groupID, currentPresidentUID));

        return Promise.all(promises).catch(error => { console.error(error) });

    } else {

        const newPresidentUID = data.newPresidentUID;

        const promises = [];
        promises.push(grantPresidentRole(newPresidentUID, groupID));
        promises.push(revokePresidentRole(currentPresidentUID, groupID));

        return Promise.all(promises)
                .then(() => {
                    return {
                        msg: "Presidente alterado com sucesso."
                    }
                })
                .catch(error => {
                    console.error(error);
                });
    }

});

async function grantPresidentRole(newPresidentUID: string, groupID: string): Promise<any> {
    return admin.auth().setCustomUserClaims(newPresidentUID, {
        isPresident: true,
        groupID: groupID
    });
}

async function grantOrganizerRole(organizerUID: string, groupID: any): Promise<void> {
    return admin.auth().setCustomUserClaims(organizerUID, {
        groupID: groupID
    });
}

async function createGroup(groupID: string, currentPresidentUID): Promise<any> {
    
    const user = await admin.auth().getUser(currentPresidentUID);

    const organizer = {uid: currentPresidentUID, 
                       email: user.email, 
                       name: user.displayName};
    
    const newGroup = {name: "Novo grupo", id: groupID, organizers: []}

    newGroup.organizers.push(organizer);    

    return admin.firestore()
            .collection('/groups')
            .doc(groupID)
            .set(newGroup);
}

async function revokePresidentRole(presidentUID: string, groupID: string): Promise<void> {
    return admin.auth().setCustomUserClaims(presidentUID, {
        isPresident: undefined,
        groupID: groupID
    });
}

exports.addOrganizer = functions.https.onCall((data, context) => {
    if(context.auth.token.isPresident === undefined) {
        return {
            msg: "Você não tem permissão para adicionar organizadores."
        }     
    }
    
    const groupID = context.auth.token.groupID;
    const organizerUID = data.organizerUID;

    return grantOrganizerRole(organizerUID, groupID)
            .then(()=>{
                return {
                    msg: "Organizador adicionado com sucesso."
                }
            });
});

exports.removeOrganizer = functions.https.onCall((data, context) => {
    if(context.auth.token.isPresident === undefined) {
        return {
            msg: "Você não tem permissão para remover organizadores."
        }     
    }

    return revokeOrganizerRole(data.organizerUID)
            .then(()=>{
                return {
                    msg: "Organizador removido com sucesso."
                }
            }); 
});

async function revokeOrganizerRole(organizerUID: string): Promise<void> {
    return admin.auth().setCustomUserClaims(organizerUID, {
        groupUID: undefined
    });
}

exports.updateLastPresence = functions
    .firestore
    .document('lists/{id}')
    .onWrite((snap:any) => {
        
        if(snap.after.data() === null || snap.after.data() === undefined){
            console.log("No data");
            return null;
        }

        const list = snap.after.data();

        const promises = [];

        if(list.members !== null && list.members !== undefined) {
            list.members.forEach(member => {
                
                member.lastPresence = list.date;
                member.status = "ativo";

                promises.push(admin
                    .firestore()
                    .collection('/members')
                    .doc(member.id)
                    .update(member));
            });
        }

        return Promise.all(promises).catch(error => console.error(error));
});

exports.scanStatus = functions.https.onCall((data, context) => {
    
    if(context.auth.token.groupID === undefined || context.auth.token.groupID === null) {
        return {
            msg: "Você não tem permissão para atualizar status."
        }     
    }

    if(data.members === undefined || data.members === null) {
        return {
            msg: "Nenhum membro selecionado."
        }
    }

    const promises = [];
    
    data.members.forEach(member => {

        if((member.oldstatus != undefined && member.oldstatus.toString().toLowerCase() !== member.status.toString().toLowerCase()) ||
            (member.oldLastPresence != undefined && member.oldLastPresence != member.lastPresence)) {
            
            promises.push(admin
                .firestore()
                .collection('/members')
                .doc(member.id)
                .update(member));

            promises.push(admin
                .firestore()
                .collection('/updatedMembers')
                .doc(member.id)
                .set(member));
        }    

    });

    return Promise.all(promises).catch(error => console.error(error));
});